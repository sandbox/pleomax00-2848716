<?php

	function wigzo_getWigzoHost () {
    /* Get current Wigzo Host */
         /* Function to check weather we are running in a Development environment, 
         file /tmp/wigzomode only exists in Dev environment */
    // if (/* check if /tmp/wigzomode exists */) {
    // return file_get_contents ("/tmp/wigzomode"); /* http://avenger.wigzopush.com */
    //	 }
    // else {
    return "https://app.wigzo.com";
    // }
}
function wigzo_gen_uuid () {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0fff ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}


function wigzo_include_wigzo_lib () {
    echo <<<EOF
    <script type="text/javascript">
	
    window.wigzoHelpers = {};
    window.wigzoHelpers.xhr = function (method, path, opts) {
        var successFn = opts.success || function () {};
        var errorFn = opts.error || function () {};
        var data = opts.data || {};

        if (method == "POST") {
            data = JSON.stringify (data);
        }

        var wxhr = typeof XMLHttpRequest != 'undefined' ? new XMLHttpRequest() : new ActiveXObject ('Microsoft.XMLHTTP');
        if (! (method == 'GET' || method == 'POST')) {
            errorFn (21, "Only GET or POST is allowed!");
            return;
        }

        if (method == "GET") {
            var out = new Array();
            for (key in data) {
                out.push (key + '=' + encodeURIComponent(data[key]));
            }
            params = out.join('&');
            path = path + "?" + params;
        }

        wxhr.open (method, path, true);
        //wxhr.setRequestHeader ("Content-Type", "application/json;charset=UTF-8"); 

        wxhr.onreadystatechange = function () {
		
            if (wxhr.readyState == 4 && wxhr.status == 200) {
                var textResp = wxhr.responseText;
                if (opts.hasOwnProperty ('expected') && opts['expected'] == 'json') {
                    try {
                        textResp = JSON.parse (textResp);
                    } catch (err) {
                        errorFn (20, "Cannot Parse JSON");
                        return;
                    }
                }
                successFn (textResp);
            }
        }

        if (method == "POST") {
			
            wxhr.send (data);
        } else {
            wxhr.send (null);
        }

    };

    </script>
EOF;
}

?>