<?php
		if ($https) {
				$gpath = array(
				  '#tag' => 'link', // The #tag is the html tag - <link />
					'#attributes' => array( // Set up an array of attributes inside the tag
					'rel' => 'manifest',
					'href' => $fname,
					),
				);
		drupal_add_html_head($gpath, 'gcm_path');
		$tracker = '<script async src="https://tracker.wigzopush.com/wigzopush_manager.js?orgtoken='.$orgid.'" type="text/javascript"></script>';
		  $track = array(
				   '#type' => 'markup',
					'#markup' => $tracker,
				);
				drupal_add_html_head($track, 'tracker_wigzopush');
		
		$trackscript = "
						<script type='text/javascript'>
						window.wigzo = (function(module){
							module.wigzoGcmAutoSubscribe = true;
							return module;
						}(window.wigzo || {}));
						</script>";
		$tracker = array(
					'#type'=>'markup',
					'#markup'=> $trackscript,
					);
		drupal_add_html_head($tracker, 'autosubscripbe');	
		}
		else
		{
			$trackorganization ='<script async src="https://tracker.wigzopush.com/gcm_http_subscribe.js?orgtoken='.$orgid.'" type="text/javascript"></script>';

			$trackorg = array(
						"#type" => "markup",
						"#markup" => $trackorganization,
						);
			drupal_add_html_head($trackorg,"track_wigzopush");
			
			$tscript="
					<script type='text/javascript'>
					window.wigzo = (function(module){
						module.httpGcmShowDialog = true;
						return module;
					}(window.wigzo || {}));
				</script>";
			$trscript = array(
						'#type'=>'markup',
						'#markup'=>$tscript,
						);	
			drupal_add_html_head($trscript,"z");
		} 
?>