<div class="wrap">
	
   <iframe id="wigzoconfigframe" style="border: 0; width: 100%; height: 600px;" src=""></iframe>
</div>

<script type="text/javascript">
var iHost;
if (window.location.port == "80" || window.location.port == "") {
    iHost = window.location.hostname;
} else {
    iHost = window.location.hostname + ":" + window.location.port;
}
document.getElementById ("wigzoconfigframe").src = "<?php  echo  wigzo_getWigzoHost(); ?>/integration/wordpress/frame/" + iHost + "/" + "<?php  echo wigzo_getAuthtoken(); ?>";

window.addEventListener ('message', function (event) { 
    var eventData = event.data;

    var aquired_token = eventData.key;
    //debugger;
    if (! aquired_token) {
        console.error ("Cannot GET static key from Wigzo!");
        return;
    }
    var params = {
        via: "xmlhttp",
        token: aquired_token,
        enabled: eventData.enabled,
        orgId: eventData.orgIdentifier,
        viahttps: eventData.viahttps
    };
    for (var k in eventData.features) {
        params[k] = eventData.features[k];
    }
    window.wigzoHelpers.xhr ('POST', '', {
            //expected: 'json',
            data: params,
            success: function (resp) {
                console.log ("Successfully, Saved Wigzo Configuration!");
            },
            error: function (code, msg) {
                console.error ("Cannot save Wigzo config: " + code + ": " + msg);
            }
    });
}); 
</script>