<?php
drupal_session_start();
include_once("lib.php.inc");
include_once("auth.php.inc");

wigzo_include_wigzo_lib ();
 
global $user;
if ( $user->uid ) {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		
       $entityBody = file_get_contents ("php://input");
		$obj = (array) json_decode($entityBody);

		$wigzo_via = db_select('system', 'n')->fields('n')->condition('filename', 'wigzo_via')->execute()->fetchAssoc();
		$wigzo_token = db_select('system', 'n')->fields('n')->condition('filename', 'wigzo_token')->execute()->fetchAssoc();
		$wigzo_enabled = db_select('system', 'n')->fields('n')->condition('filename', 'wigzo_enabled')->execute()->fetchAssoc();
		$wigzo_orgId = db_select('system', 'n')->fields('n')->condition('filename', 'wigzo_orgId')->execute()->fetchAssoc();
		$wigzo_viahttps = db_select('system', 'n')->fields('n')->condition('filename', 'wigzo_viahttps')->execute()->fetchAssoc();
		$wigzo_onsitepush = db_select('system', 'n')->fields('n')->condition('filename', 'wigzo_onsitepush')->execute()->fetchAssoc();
		
		if(!$wigzo_via['filename']|| !$wigzo_token['filename']|| !$wigzo_enabled['filename']|| !$wigzo_orgId['filename']|| !$wigzo_viahttps['filename']|| !$wigzo_onsitepush['filename'])
		{
			$wigzo_via = db_insert('system')->fields(array('filename' => 'wigzo_via','name' => $obj["via"],))
					->execute();
					
			$wigzo_token = db_insert('system')->fields(array('filename' => 'wigzo_token','name' => $obj["token"],))
					->execute();

			$wigzo_enabled = db_insert('system')->fields(array('filename' => 'wigzo_enabled','name' => $obj["enabled"],))
							->execute();	
							
			$wigzo_orgId = db_insert('system')->fields(array('filename' => 'wigzo_orgId','name' => $obj["orgId"],))
							->execute();
					
			$wigzo_viahttps = db_insert('system')->fields(array('filename' => 'wigzo_viahttps','name' => $obj["viahttps"],))
							->execute();
							
			$wigzo_onsitepush = db_insert('system')->fields(array('filename' => 'wigzo_onsitepush','name' => $obj["onsitepush"],))
								->execute();
							
			$wigzo_browserpush = db_insert('system')->fields(array('filename' => 'wigzo_browserpush','name' => $obj["browserpush"],))
								->execute();	
		}
		else{
			$wigzo_via=  db_update('system')->fields(array('filename' => 'wigzo_via','name' => $obj["via"], ))->condition('filename','wigzo_via')
						->execute();
						
			$wigzo_token=  db_update('system')->fields(array('filename' => 'wigzo_token','name' => $obj["token"], ))->condition('filename','wigzo_token')
						->execute();
			
			$wigzo_enabled=db_update('system')->fields(array('filename'=>'wigzo_enabled','name' =>$obj["enabled"],))->condition('filename','wigzo_enabled')
						->execute();
						
			$wigzo_orgId= db_update('system')->fields(array('filename'=>'wigzo_orgId','name'=>$obj["orgId"],))->condition('filename','wigzo_orgId')
						->execute();
						
			$wigzo_viahttps=db_update('system')->fields(array('filename'=>'wigzo_viahttps','name' => $obj["viahttps"],))->condition('filename','wigzo_viahttps')
						->execute();
			
			$wigzo_onsitepush=db_update('system')->fields(array('filename'=>'wigzo_onsitepush','name' => $obj["onsitepush"],))->condition('filename','wigzo_onsitepush')
						->execute();
				
		 	$wigzo_browserpush=db_update('system')->fields(array('filename'=>'wigzo_browserpush','name' => $obj["browserpush"],))->condition('filename','wigzo_browserpush')
						->execute();
						
			}	
			echo "success";
			return;		  		
	}
		include_once('wigzo_adminjs.php.inc');
	}
	else {
		  echo "Please Login by Admin";
	} 
?>