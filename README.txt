=== Wigzo - Convert & Retain more customers through Omni Channel Personalisation ===
Contributors: wigzo
Tags: wigzo mapper, wigzo, browser push, exit intent, personalization, recommendations, email personalisation, behavioural automation
Requires at least: 3.5.0
Tested up to: 4.3.1
Stable tag: 1.0.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Smart marketing automation and personalization to focus on customer engagement and growth.

== Description ==

Wigzo converts your user data into predictive insights and suggests the best marketing action.

### Track
Wigzo’s Machine Learning Tools tracks user behaviour and understands your content and builds global profiles.

### Analyse
It analyzes data to the granular level to each user their activity cross devices and builds a complex graph for each one.

### Engage
It processes the information to send them right message at the right time, on the right device with personalised notification and draws engagement.

### Predict
Wigzo predicts the churn or dormant user and users that will stick with your product

Get started today with 14 days free – no credit card required!

== Features ==

### 360 DEGREE USER PROFILING
Use all your audience data - from social media channels, to digital forums, online behaviour and more to create 360 degree user profiles. The better your understanding of users, the more personalized and effective your campaigns are.

### BEHAVIOURAL AUTOMATION
Your users behave differently, then why treat them the same way? Wigzo helps you automate your marketing processes using 360 degree user profiling. Personalized marketing automation that will guarantee you higher ROI.

### EMAIL PERSONALISATION
Wigzo automates the tedious manual workflow of email marketing. The technology focuses on creating and sending dynamically personalized emails based on behavioural data to every customer, making one-to-one communication possible.

### PERSONALIZED NOTIFICATIONS
Track user behaviour across different digital channels, understand what they’re interacting with to create notifications that encourage an action. Auto segmentation of your audience based on device and behavioural analysis.

### PRODUCT RECOMMENDATIONS
Custom product recommendations based on what adds value to your users the most, is the hack to getting higher conversions. With global profiling for each of your customers, Wigzo makes it possible to create tailored recommendation campaigns that convert more.

### EXIT INTENT
Sometimes it takes more than a compelling landing page to convert a visitor. Re-target your website visitors before they leave with personalized exit intent campaigns. Tailor the value proposition you offer, using on-site interaction data.

== OK, but why Wigzo? ==

3x customer engagement via multiple channels
34% increase in revenues
23% increase in Click Through Rate (CTR)
11% increase in Average Order Value (AOV)

== Installation ==

Before proceeding please make sure that you are running Drupal 7.

Please refer to the Drupal documentation on how to get the module appear in your installation admin section.

Once the module appears in your installation, you must activate it. Navigate to the "Modules" section and locate the "Wigzo" module. The activation is done by simply clicking the checkbox next to Wigzo Module and by clicking the Save Configuration button

Once you have activated the module and added the necessary actions, you need to configure the module. The module configuration page can be found under "Navigation > Wigzo".

Once the module is activated campaigns and there respective reports can be managed via http://app.wigzo.com/


The configuration page consists of two settings:

* Browser Push
	* Http / Https (choose https if your website have a valid SSL certificate running on HTTPS. If you don't own any certificate, you can select HTTP.)
* On-Site Push Notification : Enable or disable to activate the same

All of the above settings are needed for the plugin to work. You do not need to add Wigzo javascript manually to your store.


The plugin uses the Drupal Action API to add content to the website. However, there are a few actions that will have to be added to the Drupal theme in order for the module to function to its full extent.

* Tracking/sending event can be done using our API
	* wigzo.track(eventName, eventValue);
* Users can be mapped using our API
	* wigzo.identify(userInfo, force, callback);
* Indexing product using our API. It's recommended to use this API only if you are following SPA(Single page architecture).
	* wigzo.index(productInfo);
	
For brief info do visit Wigzo Integrations Page i.e https://app.wigzo.com/integration/apicodeintegration	


== Changelog ==

= 1.0.0 =
* Initial release

== Screenshots ==

1. The real-time Wigzo admin dashboard for a clear overview
2. Machine Learning powered Exit Intent 
3. Personalised Browser Push notifications
4. Personalised On-Site Push Notification
5. Abandoned Cart and Price Drop Push alert / Personalized Triggered Emails